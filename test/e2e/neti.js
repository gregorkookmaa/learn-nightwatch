/*

[X] open neti.ee
[X] pealkiri on "NETI - Eesti Interneti Kataloog ja Otsingusüsteem"
[X] vota screenshot
[X] Valib Tehnika ja Ehituse kategooria
[X] vota screenshot
[X] Ootab kuni erinevad valikud on nähtaval
[X] Valib Elektroonika
[X] vota screenshot
[X] Valib Arvutite Kategooria
[X] Vota screenshot
[X] Valib E-Poodide alamkategooria
[X] Vota screenshot
[X] Valib Arvutitarga lehe
[X] Ootab kuni Arvutitarga leht on laetud
[X] vota screenshot
[X] Pealkiri on "Arvutitark.ee - Eripakkumised"

*/

const config = require('../../nightwatch.conf.js');

module.exports = {
  'Neti --> Arvutitark testing': function (browser) {
    browser
      .url('https://www.neti.ee/')
      .waitForElementVisible('body', 2000)
      .windowSize('current', 1600, 900)
      .assert.title('NETI - Eesti Interneti Kataloog ja Otsingusüsteem')
      .saveScreenshot(`${config.imgpath(browser)}neti-front-page.png`)
      .click('div.avaleht:nth-child(2) > h2:nth-child(5) > a:nth-child(1)') // Valib "Tehnika ja Ehitus"
      .saveScreenshot(`${config.imgpath(browser)}tehnika_Ehitus.png`)
      .waitForElementVisible('div.main-content')
      .click('.right > li:nth-child(34) > a:nth-child(1)') // Valib "Elektroonika"
      .saveScreenshot(`${config.imgpath(browser)}Elektroonika.png`)
      .click('.right > li:nth-child(1) > a:nth-child(1)') // Valib Arvutite kategooria
      .saveScreenshot(`${config.imgpath(browser)}Arvutid.png`)
      .click('div.tab-content:nth-child(1) > ul:nth-child(1) > li:nth-child(4) > a:nth-child(1)') // Valib E-poodide alamkategooria
      .saveScreenshot(`${config.imgpath(browser)}E-poed.png`)
      .click('li.result-item:nth-child(69) > h3:nth-child(1)') // Valib Arvutitarga lehe
      .saveScreenshot(`${config.imgpath(browser)}Arvutitark_Front_Page.png`)
      .waitForElementVisible('body', 2000)
      .assert.title('Arvutitark.ee - Eripakkumised')
      .end();
  },
};
