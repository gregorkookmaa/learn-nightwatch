/*

ava https://nightwatchjs.org/
kontrolli kas pealkiri on "Nightwatch.js |  Node.js powered End-to-End testing framework"
  * kontrolli hoopis et algab "Ni ghtwatch.js"
vota screenshot
ava "Developer Guide"
vaata et lehe peal oleks nahtav pealkiri "Writing Tests"
vota screenshot

*/
const config = require('../../nightwatch.conf.js');

module.exports = {
  Nightwatch(browser) {
    browser
      .url('https://nightwatchjs.org/')
      .windowSize('current', 1000, 1000)
      .waitForElementVisible('body')
      .assert.title('Nightwatch.js | Node.js powered End-to-End testing framework')
      .saveScreenshot(`${config.imgpath(browser)}nightwatch.png`)
      .assert.containsText('#navigation', 'Developer Guide')
      .useXpath()
      .click('//*[@id="navigation"]/ul/li/a[text()="Developer Guide"]')
      .pause(500)
      .useCss()
      .waitForElementVisible('body')
      .assert.containsText('body', 'Writing Tests')
      .saveScreenshot(`${config.imgpath(browser)}nightwatch-developer-guide.png`)
      .end();
  },
};
